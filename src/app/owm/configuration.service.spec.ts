import { TestBed, inject } from '@angular/core/testing';

import { OWMService } from './configuration.service';

describe('OWMService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OWMService]
    });
  });

  it('should be created', inject([OWMService], (service: OWMService) => {
    expect(service).toBeTruthy();
  }));
});
