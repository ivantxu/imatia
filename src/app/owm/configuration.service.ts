import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import * as _ from 'lodash';

@Injectable()
export class OWMService {
  public Server = 'http://api.openweathermap.org/data/2.5/';
  public ApiKey = '6955538c21de54f621430f309ec3c14a';

  getCities(cityArray): Observable<any> {
    let cityList = '';

    _.each(cityArray, function (cityItem) {
      cityList = cityList.length > 0 ? cityList + ',' + cityItem : cityItem;
    });

    return this.http.get(this.Server + 'group?id=' + cityList + '&units=metric' + '&APPID=' + this.ApiKey);
  }

  getHourlyForecast(cityId): Observable<any> {
    return this.http.get(this.Server + 'forecast?id=' + cityId + '&units=metric' + '&APPID=' + this.ApiKey);
  }

  findCity(query): Observable<any> {
    return this.http.get(this.Server + 'find?q=' + query + '&units=metric' + '&APPID=' + this.ApiKey);
  }

  constructor(private http: HttpClient) {
    this.http = http;
  }
}
