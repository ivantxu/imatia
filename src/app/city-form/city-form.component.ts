import {Component, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import {OWMService} from "../owm/configuration.service";

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})
export class CityFormComponent implements OnInit, OnDestroy {

  @Output() select = new EventEmitter<any>();

  private model;
  private cities = [];
  private findCitySubscription;

  onSubmit() {
    this.findCitySubscription = this.OWM.findCity(this.model).subscribe(data => this.cities = data.list);
  }

  onCitySelect(event) {
    this.select.emit(event);
  }

  constructor(private OWM: OWMService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.findCitySubscription) {
      this.findCitySubscription.unsubscribe();
    }
  }

}
