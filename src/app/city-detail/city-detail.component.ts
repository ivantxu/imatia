import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router'

import {OWMService} from "../owm/configuration.service";
import * as moment from "moment";
import * as _ from "lodash";
import {EmojiService} from "../emoji/emoji.service";

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.css'],
  providers: [EmojiService]
})
export class CityDetailComponent implements OnInit, OnDestroy {

  private city;
  private cityId;
  private cityHourlyForecast;
  private charts;
  private subscriptions = [];

  static buildChartData(chartTitle, chartId, series, labels, options, emojis) {
    return {
      'chartTitle': chartTitle,
      'chartId': chartId,
      'chartData': {
        'labels': labels,
        'series': [series],
        'fullWidth': true,
      },
      'options': options,
      'emojis': emojis
    };
  }

  constructor(private route: ActivatedRoute, private OWM: OWMService, private Emoji: EmojiService) {
  }

  ngOnInit() {
    let that = this;
    let temp = [null,];
    let hours = [null,];
    let emojis = [null,];
    let rain = [null,];
    let humidity = [null,];
    let getCitiesSubscription;
    let getHourlySubscription;
    this.cityId = this.route.snapshot.params.id;

    getCitiesSubscription = this.OWM.getCities([this.cityId]).subscribe(data => {
      this.city = data.list[0];
    });
    this.subscriptions.push(getCitiesSubscription);

    getHourlySubscription = this.OWM.getHourlyForecast(this.cityId).subscribe(data => {
      this.cityHourlyForecast = data.list;
      _.each(this.cityHourlyForecast.slice(0, 32), function (hour) {
        temp.push(Math.round(hour.main.temp));
        rain.push(!_.isUndefined(hour) && !_.isUndefined(hour.rain) && !_.isUndefined(hour.rain['3h']) ? hour.rain['3h'] : 0);
        humidity.push(hour.main.humidity);
        hours.push(hour ? moment.unix(hour.dt).local().format('D/MM H:mm') : null);
        emojis.push(that.Emoji.getEmoji(hour.weather[0].icon));
      });
        this.subscriptions.push(getHourlySubscription);

      let graphOptionsTemp = {
        low: _.min(temp) - 1,
        high: _.max(temp) + 1,
        axisY: {
          onlyInteger: true,
          offset: 1
        }
      };

      // Chart data as input for the climate-charts component
      this.charts = [
        CityDetailComponent.buildChartData('Temperatura', 'temp-graph', temp, hours, graphOptionsTemp, emojis),
        CityDetailComponent.buildChartData('Lluvia', 'rain-graph', rain, hours, {}, undefined),
        CityDetailComponent.buildChartData('Humedad', 'humidity-graph', humidity, hours, {}, undefined)
      ]

    });

  }

  ngOnDestroy() {
    _.each(this.subscriptions, function (subscription) {
      subscription.unsubscribe();
    })
  }
}
