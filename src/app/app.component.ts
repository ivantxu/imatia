import {Component, OnInit} from '@angular/core';
import {OWMService} from "./owm/configuration.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [OWMService]
})
export class AppComponent implements OnInit {

  ngOnInit(): void {

  }

  constructor(private OWM: OWMService) {
  }
}
