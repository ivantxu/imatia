import {Component, OnDestroy, OnInit} from '@angular/core';
import {OWMService} from "../owm/configuration.service";
import {Chart} from 'chart.js';
import * as _ from "lodash";

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css'],
})
export class CityListComponent implements OnInit, OnDestroy {

  private capitalIds;
  private capitals;
  private subscriptions = [];

  loadCities(ids) {
    let getCitiesSubscription = this.OWM.getCities(ids).subscribe(data => this.capitals = data.list);
    this.subscriptions.push(getCitiesSubscription);
  }

  addCity(event) {
    this.capitalIds.push(event.city.id.toString());
    this.loadCities(this.capitalIds);
    localStorage.setItem('capitals', JSON.stringify(this.capitalIds));

  }

  deleteCity(event) {
    console.log(event);
    this.capitals = _.filter(this.capitals, function(capital){
      return capital.id !== event.cityId;
    });
    this.capitalIds = _.filter(this.capitalIds, function(capitalId){
      console.log(capitalId, event.cityId)
      return capitalId !== event.cityId.toString();
    });
    console.log(this.capitals, this.capitalIds);
    localStorage.setItem('capitals', JSON.stringify(this.capitalIds));
  }

  ngOnInit(): void {
    let getCitiesSubscription = this.OWM.getCities(this.capitalIds).subscribe(data => this.capitals = data.list);
    this.subscriptions.push(getCitiesSubscription);
    this.loadCities(this.capitalIds);
  }

  ngOnDestroy() {
    _.each(this.subscriptions, function (subscription) {
      subscription.unsubscribe();
    })
  }

  constructor(private OWM: OWMService) {
    let storedCapitals = localStorage.getItem('capitals');
    if (storedCapitals === null) {
      let defaultCapitals = ['3119841', '3117814', '3113209', '3114965'];
      localStorage.setItem('capitals', JSON.stringify(defaultCapitals));
      this.capitalIds = defaultCapitals;
    } else {
      let unparsedCapitals = storedCapitals[0] === '[' ? storedCapitals: '[' + storedCapitals + ']';
      this.capitalIds = JSON.parse(unparsedCapitals);
    }
  }

}
