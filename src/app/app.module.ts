import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RouterModule, Routes} from "@angular/router";

import { HttpClientModule } from '@angular/common/http';
import { CityCardsComponent } from './city-cards/city-cards.component';
import { CityListComponent } from './city-list/city-list.component';
import { CityDetailComponent } from './city-detail/city-detail.component';
import {ChartsModule} from "ng2-charts";
import { CityFormComponent } from './city-form/city-form.component';
import {FormsModule} from "@angular/forms";
import { CityResultsComponent } from './city-results/city-results.component';
import { ClimateChartsComponent } from './climate-charts/climate-charts.component';

const appRoutes: Routes = [
  { path: '' , redirectTo: 'principal', pathMatch: 'full'},
  { path: 'principal', component: CityListComponent },
  { path: 'detalle/:id', component: CityDetailComponent },
  { path: '**', redirectTo: 'principal'}
];

@NgModule({
  declarations: [
    AppComponent,
    CityCardsComponent,
    CityListComponent,
    CityDetailComponent,
    CityFormComponent,
    CityResultsComponent,
    ClimateChartsComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
