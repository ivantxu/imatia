import {Component, OnInit, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {EmojiService} from "../emoji/emoji.service";

@Component({
  selector: 'app-city-cards',
  templateUrl: './city-cards.component.html',
  styleUrls: ['./city-cards.component.css'],
  providers: [EmojiService]
})
export class CityCardsComponent implements OnInit, OnChanges {

  @Input() city: any;
  @Output() erase = new EventEmitter<any>();

  private sunrise;
  private sunset;
  private windSpeed;
  private icon;
  private temp;

  constructor(private router: Router, private Emoji: EmojiService) {
  }

  onDeleteCity() {
    this.erase.emit({'cityId': this.city.id});
  }

  ngOnChanges() {
  }

  goToDetail() {
    this.router.navigate(['detalle', this.city.id])
  }

  ngOnInit() {
    this.sunrise = CityCardsComponent.getHour(this.city.sys.sunrise);
    this.sunset = CityCardsComponent.getHour(this.city.sys.sunset);
    this.icon = this.Emoji.getEmoji(this.city.weather[0].icon);
    this.windSpeed = Math.round(this.city.wind.speed);
    this.temp = Math.round(this.city.main.temp);
  }

  static getHour(unixTime) {
    return moment.unix(unixTime).local().format('H:mm')
  }

}
