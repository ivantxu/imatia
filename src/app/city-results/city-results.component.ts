import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-city-results',
  templateUrl: './city-results.component.html',
  styleUrls: ['./city-results.component.css']
})
export class CityResultsComponent implements OnInit {
  @Input() cities: any;
  @Output() select = new EventEmitter<any>();

  selectCity(city) {
    this.select.emit({
      city: city
    });
  }
  constructor() { }

  ngOnInit() {
  }

}
