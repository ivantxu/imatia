import {Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-climate-charts',
  templateUrl: './climate-charts.component.html',
  styleUrls: ['./climate-charts.component.css']
})
export class ClimateChartsComponent implements OnInit, OnChanges {

  @Input() data: any;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    let that = this;
    let selector = '#' + this.data.chartId;
    // A 0 timeout is needed so the id gets rendered on the element and we can start Chartist
    setTimeout(() => {
      let chart = new Chartist.Line(selector, this.data.chartData, this.data.options);

      // Drawing weather emojis on the graph
      if (this.data.emojis) {
        chart.on('draw', function (data) {
          if (data.type === 'point' && that.data.emojis[data.index]) {

            data.group.elem('text', {
              x: data.x - 20,
              y: data.y + 10,
              transform: 'rotate(0, ' + data.x + ', ' + data.y + ')'
            }, 'ct-label').text(that.data.emojis[data.index]);
          }
        });
      }
    }, 0);
  }

}
