import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClimateChartsComponent } from './climate-charts.component';

describe('ClimateChartsComponent', () => {
  let component: ClimateChartsComponent;
  let fixture: ComponentFixture<ClimateChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClimateChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClimateChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
